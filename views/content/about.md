---
tags: about
permalink: about.html
layout: singlepage.njk
---


Inachevé d'imprimer est un espace d’échanges consacrés à la publication hybride, en particulier lorsqu'elle se fait expérimentale, systémique et collaborative.

- GitLab : [https://gitlab.com/inacheve-dimprimer](https://gitlab.com/inacheve-dimprimer)
- Are.na : [https://www.are.na/pubnerds-inacheve-d-imprimer](https://www.are.na/pubnerds-inacheve-d-imprimer)
- Zotero : [https://www.zotero.org/groups/2752425/](https://www.zotero.org/groups/2752425/)
- Rejoindre la liste de diffusion : [https://groupes.renater.fr/sympa/subscribe/inacheve-dimprimer](https://groupes.renater.fr/sympa/subscribe/inacheve-dimprimer?previous_action=info)