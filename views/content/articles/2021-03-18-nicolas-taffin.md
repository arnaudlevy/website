--- 
title: "*Paged.js* écriture & mise en page"
author: Arthur Perret
invite: Nicolas Taffin 
# subtitle: Nicolas Taffin
portrait: "/images/taffin.jpeg"
date: 2021-03-18
layout: cr.njk
tags: 
- compte-rendu 
- articles
visiourl: https://bigbluebutton.cloud68.co/b/jul-p0g-nnn-sl3
intro: "Nicolas Taffin présente son utilisation de Paged.js pour la conception d'un livre dont les contenus se déploient de manière complexe sur la double page et dans les marges. C'est aussi le récit d'une collaboration entre auteur et designer qui façonne l'outillage éditorial, et vice-versa."
permalink: articles/2021-03-18-nicolas-taffin.html
customcss: "paginated.css"
class: paginated
invitebio: "[Nicolas Taffin](https://polylogue.org/a-propos/) est concepteur graphique et éditeur (cofondateur de [C&F éditions](https://cfeditions.com/public/) avec Hervé Le Crosnier). Il exerce dans les champs culturel et scientifique (équipe [ex)situ](https://ex-situ.lri.fr/) de l'Inria) et se partage entre les images, les lettres et quelques secrets depuis ses études de philosophie. Enseignant associé au master édition à l’[Université de Caen-Normandie](http://ufrhss.unicaen.fr/formations/master/master-metiers-du-livre-et-de-l-edition/) (2013-2019), il a également, au cours des dernières années, dirigé le design d’applications d’archives du web et de navigation dans l’information chez [Internet Memory Research](http://internetmemory.org/), et présidé les [Rencontres internationales de Lure](http://delure.org/)."
# invitepic: nicolas-taffin.jpg
---

**C&F** est une maison d'édition spécialisée dans les thématiques des communs de la connaissance et de la culture numérique. Jusqu'à une période récente, l'essentiel du travail éditorial reposait sur des outils de PAO classiques – essentiellement InDesign, et quelques incursions plus ou moins probantes chez la concurrence (Scribus). Mais la culture du libre portée notamment par Hervé Le Crosnier chez C&F a fini par infuser dans les pratiques éditoriales, et la question d'un changement de chaîne de production s'est posée, notamment pour y intégrer d'autres acteurs que le designer éditorial : comment faire intervenir les non-techniciens du livre ailleurs qu'en amont de la production ? Peut-on repenser le processus de fabrication pour y associer auteurs mais aussi correcteurs ou traducteurs de manière plus synchrone ?

Nicolas s'est frotté à ces questions en choisissant de tester Paged.js pour créer une nouvelle collection nommée Interventions, composée de livres simples : page de titre, chapitrage, une colonne de texte, éventuellement quelques intercalaires. Il relate le making-of de cette collection [sur son site](https://polylogue.org/addictions-sur-ordonnance-making-of-dune-collection-liberee/). Sa réutilisation de Paged.js a occasionné la création d'outils sur mesure, immédiatement redistribués à la communauté, comme le rechargement du livre au bon endroit (*reload in-place*). L'idée de Nicolas était de s'emparer d'une technologie prometteuse et au passage d'en améliorer l'*interfaçage* pour disposer d'un outillage efficace et confortable.

Depuis trois ans, Nicolas participe aux travaux de l'équipe de recherche Inria Ex-situ, spécialisée dans les questions d'interaction homme-machine. Il travaille notamment sur un projet de manuel rédigé par Wendy Mackay (directrice de l'équipe) et sur lequel il a pu continuer son expérimentation avec Paged.js. Le matériau s'avérait particulièrement complexe : double page, résumés, mots-clés, figures avec légendes, encadrés, listes, notes… Et dans les marges, des éléments censés se comporter différemment : légendes alignées sur la figure, éléments alignés sur le coin, éléments flottants, éléments qui débordent de la colonne sur la marge, etc. La rédaction du livre a commencé dans Word mais le paradigme du traitement de texte s'est rapidement révélé insuffisant : impossible de prévisualiser ce que donnerait le contenu une fois mis en page, et donc d'écrire en fonction du médium. Changement d'approche donc, et transformation de la logique de travail, d'écriture et d'édition suivant une logique que Nicolas décrit dans un second making-of [publié sur son site](https://polylogue.org/apres-la-page-la-double-page/).

L'enjeu de cette nouvelle expérience avec Paged.js n'était pas seulement de développer une maquette adaptée à un format beaucoup plus complexe. De l'espace linéaire du manuscrit dans l'éditeur de code à l'espace graphique de la double page dans le navigateur, il s'agissait aussi de bâtir un terrain de jeu qui permette à l'auteur et au designer de travailler sans se marcher dessus. Un travail à quatre mains qui implique au final un certain nombre d'outils et de processus :

- Excel : des parties entières du manuscrit étaient rédigées dans Excel, ce qui n'est pas évident a priori mais permet notamment d'homogénéiser beaucoup de choses.
- Drive, Dropbox, Nextcloud : des infrastructures de stockage en ligne et de synchronisation classiques, préférées aux plateformes basées sur git (voir la discussion sur la collaboration plus loin).
- Asciidoc : bien qu'il apprécie Markdown, Nicolas a opté pour cette autre syntaxe de balisage léger pour la rédaction du texte, mieux adaptée à un manuel (qui contient de nombreuses formes codifiées) et extensible par nature. La capacité de l'auteur à s'emparer de la syntaxe a bien entendu été déterminante.
- Des données injectées dans le manuscrit depuis des fichiers externes, par exemple des données en CSV.
- Visual Studio Code : éditeur choisi notamment pour sa capacité à compiler automatique le rendu à l'enregistrement des fichiers.

Ces différentes briques forment un système qui obéit à une contrainte fondamentale : auteur et designer doivent voir le même livre en train de se construire. Nicolas a pris des décisions qui compliquent sa tâche mais rendent l'écriture beaucoup plus transparente pour l'auteur : le balisage est purement sémantique, aucune classe CSS « de designer » n'est employée dans le manuscrit. Tout ne se fait pas automatiquement du premier coup : d'abord l'auteur écrit, prévisualise avec le designer ; puis interviennent des corrections et retouches de l'auteur, et la modification de la maquette par le designer. Mais cette dernière n'occasionne pas d'intervention sur le texte : Nicolas passe par une seconde feuille de style qui contient les ajustements. En théorie, le processus se fait donc en deux temps ; dans les faits, il y a de nombreux « temps » qui sont autant de tâtonnements.

Sur ce projet, une grande partie du travail de design a consisté à passer du manuscrit, dans lequel les contenus se suivent de manière linéaire, à une double page sur laquelle les contenus viennent flotter ou s'ancrer. Nicolas s'est donné pour ambition de gérer les éléments flottants aussi bien que LaTeX, le vénérable système de composition typographique auxquels ses interlocuteurs de l'Inria sont habitués. Là aussi, de la théorie à la pratique il y a plus d'un pas : cette gestion est possible, comme le démontrent certains prototypes dans le GitLab de Paged.js, mais ce n'est pas si facile que ça. La manière dont Paged.js gère le sectionnement et déplace des éléments de manière dynamique nécessite de ruser : Nicolas passe par un pré-traitement (injection d'identifiants qui lui permettent de mieux maîtriser le sectionnement), des *hooks*, des propriétés personnalisées, des feuilles de styles modulaires qui s'appellent les unes les autres, le tout en insérant des scripts via un template AsciiDoc. Un travail impressionnant qui dépasse clairement la simple réutilisation du *polyfill* Paged.js, pour un résultat imparfait (d'après Nicolas, modeste) mais riche d'enseignements.

## Discussion

À la question d'[Anthony Masure](https://www.anthonymasure.com), qui s'interrogeait sur les choix techniques de Nicolas en matière de collaboration, l'intéressé répond en relatant ses déboires avec Git, outil puissant mais rigide, qui nécessite peut-être une trop grande discipline. Le choix s'est porté sur Nextcloud, solution plus simple mais qui nécessite aussi une forme de discipline (laquelle s'applique de toute façon aussi à Git) : ne pas modifier les mêmes régions du même fichier en même temps, travailler dans des fichiers distincts (notamment pour les traductions).

Nicolas en profite pour évoquer son environnement de travail rêvé pour ce type de projet : un éditeur collaboratif en ligne qui rassemble auteur, designer, correcteur, traducteur… [Antoine Fauchié](https://www.quaternum.net/a-propos/) et Julien Taquet nous rappellent que des outils de ce type existent – via Forestry, VS Code – mais que la difficulté n'est pas tellement dans l'éditeur plutôt que dans la gestion des permissions et des conflits d'édition.

***

[Nolwenn Maudet](https://nolwennmaudet.com) questionne Nicolas sur la collaboration éditoriale : comment l'autrice fait-elle pour signaler des envies de modifications dans la mise en page ?

Nicolas répond en décrivant la conversation qu'il entretient avec Wendy Mackay autour des besoins de design : son rôle est de puiser dans cette discussion pour créer des solutions dont l'autrice pourra s'emparer ensuite. L'objectif est de créer une palette suffisante pour l'expression des deux métiers, plutôt que la réduire à un plus petit dénominateur commun. Chaque contexte métier ajoute une couche supplémentaire : ainsi la réflexion serait renouvelée en présence de correcteurs ou de traducteurs, par exemple.

***

Partant de l'expérience récente d'un [atelier de formation par la recherche](http://reticulum.info/3/) ayant mobilisé, entre autres outils, Paged.js, Florian Harmand demande à Nicolas son avis sur les pratiques liées à CSS, du *reset* aux bibliothèques.

La réponse peut être reformulée ainsi : tout cramer, pour repartir sur des bases saines. Virer les appels CDN, les dépendances, tout. Simplifier, pour garder le contrôle. C'est ici que la culture éditoriale livresque marque sa spécificité : le livre papier est un univers clos, non connecté, qui tolère beaucoup moins les défauts de qualité que le web. Sans aller jusqu'à parler d'une forme d'ascèse bénéfique, au moins peut-elle inspirer celles et ceux qui conçoivent des objets éditoraux via les outils du web. Derrière cette interrogation, on devine la question plus générale de la dépendance à une surcouche logicielle dont l'utilité doit être régulièrement ré-interrogée.

***

Antoine revient sur la question de la collaboration : la réalité du terrain, c'est que les designers et les auteurs travaillent souvent avec des développeurs qui veulent avancer vite et efficacement. Difficile alors de trouver un compromis parfait pour la complexité des besoins mis en jeu. À quel moment le prototype devient-il très (trop) complexe, nécessitant de passer à la production et de réduire le nombre de personnes impliquées ?

Nicolas évoque la possibilité de gérer la complexité avec un système d'unités éditoriales, inspiré par son expérience avec la chaîne XML [Métopes](http://www.metopes.fr/metopes.html). Faire un fichier AsciiDoc (ou Markdown) par chapitre, des CSS génériques et spécifiques à chaque unité éditoriale : atomiser le contenu et séparer autant que possible les langages techniques.

***

Pour finir, Ouidade Soussi évoque une question amusante : en généralisant la méthode décrite par Nicolas, les éditeurs seraient-ils seulement conscients qu'il n'y a plus de fichier InDesign ou Scribus ? Nicolas confirme que non.

La discussion glisse rapidement sur les risques de cette expérimentation. Pour Julie Blanc, il existe un réel danger que certains éditeurs se représentent ces technologies comme un moyen de tout automatiser, alors que la seule automatisation concerne le rendu – qui est similaire au rendu typographique dans InDesign. Ouidade évoque aussi le risque d'une injonction faite aux graphistes de devenir développeurs. Pour Nicolas, il faut plutôt considérer l'opportunité que représente ces explorations techniques pour un designer : ça implique de se demander ce qu'on veut. Partir d'une page blanche et y ajouter, en réfléchissant à sa pratique, en se remettant en question. Tout le contraire d'une interface pousse-boutons.





<span class="credit">L’article est écrit par les pubnerds et mis en forme par Julien Taquet. Composé en Fraunces (caractère dessiné par Phaedra Charles and Flavia Zimbardi d’Undercase Type) sous licence OFL. 