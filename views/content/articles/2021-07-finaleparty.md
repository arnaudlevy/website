--- 
title: "Fin de saison party"
subtitle: Une dernière pour la route (avant septembre)
author: l’équipe
invite: 'Tout le monde'
date: 2021-07-08
nodate: false
tags: 
- articles
visiourl: https://bigbluebutton.cloud68.co/b/jul-p0g-nnn-sl3
intro: Avant que l'été ne s'installe définitivement, en absence d'invité particulier, faisons un petit bilan et des partages expérimentaux impromptus, et une prise de notes collective sur nos envies pour les futures sessions
---  

Une session Inachevé d'imprimer "fin de saison party" avant que l'été ne s'installe définitivement.
Pas d'invité particulier mais un petit bilan, des partages expérimentaux impromptus, et aussi une prise de notes collective sur nos envies pour les futures sessions. 
