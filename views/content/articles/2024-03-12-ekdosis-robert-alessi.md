--- 
title: "Des éditions critiques avec Ekdosis : au carrefour de l'imprimé et du numérique"
author: Antoine Fauchié
invite: "Robert Alessi"
portrait: ""
date: 2024-03-12
dateprecise: "Rencontre en ligne mardi 12 mars 2024 à 16h (UTC+01:00)"
nodate: false
layout: cr.njk
tags: 
- prochain rendez-vous
- articles
visiourl: "https://bbb.futuretic.fr/rooms/ti8-xxt-ao0-xxg/join"
intro: "Robert Alessi développe et maintient Ekdosis (https://www.ekdosis.org), un paquet LuaLaTeX pour l'édition critique de textes anciens. Ekdosis est conçu pour faciliter le balisage et la composition des éditions critiques, et pour obtenir un format paginé PDF ainsi qu'un format numérique XML-TEI. Ekdosis conjugue une modélisation imprimée et une modélisation numérique. Robert Alessi est maître de conférence (Unité Mixte de Recherche \"Orient & Méditerranée\" (CNRS UMR 8167, Paris))."
permalink: articles/2024-03-12-ekdosis-robert-alessi.html
customcss: "paginated.css"
class: paginated
invitebio: ""
invitepic: ""
---
