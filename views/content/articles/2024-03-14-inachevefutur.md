---
title: "Futurs inachevés"
author: "nous tous"
invite: "Antoine Fauchié, Arthur Perret & julien taquet"
portrait: ""
date: 2024-03-14
nodate: false
layout: cr.njk
tags:
  - prochain rendez-vous
  - articles
visiourl: "https://marsha.education/my-contents/classroom/c8d437c9-5202-45b4-b2c3-d2d9429bd6ff/invite/VBiRRu_pw6B-Jui7bGqtjTEkd6NhyrCXWu-eCCJGLSc"
intro: "Depuis 3 ans, on discute ensemble d’outillages, de pratiques et d’expérimentations de publications. Profitant de la reprise des rencontres après une année assez douce, nous proposons un temps d'échange pour discuter ensemble : partager des idées d’invitation pour les rencontres à venir, discuter du fonctionnement (simple) du collectif, proposer des événements ou tout simplement discuter entre le café et le thé."
permalink: articles/2024-03-14-inachevés-futurs.html
customcss: "paginated.css"
class: paginated
invitebio: ""
invitepic: ""
---
