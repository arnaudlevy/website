--- 
title: "Rencontre avec Aurore Turbiau"
author: Arthur Perret
invite: ""
portrait: ""
date: 2024-06-11
dateprecise: "En ligne le mardi 11 juin 2024 à 16h (UTC+01:00)"
nodate: false
layout: cr.njk
tags: 
- prochain rendez-vous
- articles
visiourl: "https://marsha.education/my-contents/classroom/ff98a156-fc2c-4852-9e61-98c936cd12c3/invite/rdbxlMpcRUhzJA7CnGklS-VA2dTphk1-3jB63jQO6qY"
intro: "Aurore Turbiau est l'autrice d'une thèse sur l'engagement littéraire en lien avec le féminisme. Elle a également créé Litote, un logiciel de gestion de corpus et d'aide à la publication. Avec elle, nous parlerons (entre autres) d'invention typographique, du fait de fabriquer ses propres outils, d'hypertextualité, de citation…"
permalink: articles/2024-06-11-aurore-turbiau.html
customcss: "paginated.css"
class: paginated
invitebio: ""
invitepic: ""
---
