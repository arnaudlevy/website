---
layout: home.njk
permalink: /index.html 
class: index
footer: >
    Le site d’*Innachevé d’imprimé* est composé en [Newsreader](https://www.productiontype.com/news/new_fonts_from_production_type_newsreader) dessiné et développé par Hugues Gentile et Jean-Baptiste Levée de Production Type), est généré grâce à [Eleventy](https://www.11ty.dev/) et est hebergé sur [Gitlab](https://gitlab.com). Le Big blue button qui sert pour les rencontres est hébergé par la merveilleuse équipe de [Cloud68.co](https://cloud68.co/).
---

> Tout le contraire d'une interface pousse-boutons.
> <cite><span class="smallcap">Nicolas Taffin</span>

